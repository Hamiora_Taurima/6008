﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Math_problem
{
    class Class1
    {
        private string Input;

        public Class1(string _input)
        {
            Input = _input;
        }

        public int SumOfValues()
        {
            var sum = 0;
            var output = 0;

            bool isInt = int.TryParse(Input, out output);

            if (isInt)
            {
                for (int i = 0; i < output; i++)
                {
                    if (i % 3 == 0 || i % 5 == 0)
                    {
                        sum = sum + i;
                    }
                }
            }
            else
            {
                sum = 4008;
            }

            return sum;
        }
    }   
}
