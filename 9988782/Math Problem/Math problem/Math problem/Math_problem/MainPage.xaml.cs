﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Math_problem
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
        }

        public void OnButtonClicked(object sender, EventArgs e)
        {
            var clicked = new Class1(UserInput.Text);

            var result = clicked.SumOfValues();

            ResultsLabel.Text = ($"{result}");



        }

    }
