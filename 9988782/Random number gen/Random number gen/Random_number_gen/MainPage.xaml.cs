﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Random_number_gen
{
	public partial class MainPage : ContentPage
	{
        int correct = 0;
        int total = 0;

        public MainPage()
        {
            InitializeComponent();
        }

        public void OnButtonClicked(object sender, EventArgs e)
        {

            int output;


            if (int.TryParse(Class1.Text, out output))
            {
                bool result = Class1.ranGen(output);

                if (result == true)
                {


                    labelColor.BackgroundColor = Color.Green;

                    labelTag.Text = "Correct";
                    correct++;
                }
                else
                {

                    labelColor.BackgroundColor = Color.Red;

                    labelTag.Text = "Incorrect";
                }


                total++;



                labelTotal.Text = total.ToString();
                labelCorrect.Text = correct.ToString();
            }
        }